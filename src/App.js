import React from 'react';
import { Switch, Route, HashRouter } from 'react-router-dom';
import { Footer } from './components';
import GlobalStyle from './globalStyles';
import Home from './pages/HomePage/Home';
import SearchGames from './pages/SearchGames/SearchGames';
import PopularGames from './pages/PopularGames/PopularGames';
import NewGames from './pages/NewGames/NewGames';
import ScrollToTop from './components/ScrollToTop';
function App() {
  return (
    <HashRouter basename={process.env.PUBLIC_URL}>
      <GlobalStyle />
      <ScrollToTop />
      <Switch>
        <Route path='/' exact component={Home} />

        <Route path='/search-game'>
          <SearchGames />
        </Route>

        <Route path={['/game/:id', '/popular-games']}>
          <PopularGames />
        </Route>

        <Route path={['/game/:id', '/new-games']}>
          <NewGames />
        </Route>
      </Switch>
      <Footer />
    </HashRouter>
  );
}

export default App;
